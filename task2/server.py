import socket
import json
import math

def trapezeArea(a, b, c, d):
    return (a + b) * math.sqrt(c ** 2 - (((a - b) ** 2 + c ** 2 - d ** 2) / (2 * (a - b))) ** 2) / 2

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('localhost', 9091))
sock.listen(1)

clientsocket, address = sock.accept()
data = json.loads(clientsocket.recv(2048))
area = trapezeArea(data['a'], data['b'], data['c'], data['d'])
clientsocket.send(str(area).encode('UTF-8'))
clientsocket.close()
sock.close()
