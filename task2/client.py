import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('localhost', 9091))

sock.send(b'{ "a": 4, "b": 2, "c": 2, "d": 2.828 }')
print(sock.recv(1024).decode('UTF-8'))
sock.close()