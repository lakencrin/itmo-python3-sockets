import socket
import time
import threading

class MessageList:
    def __init__(self):
        self.lock = threading.Lock()
        self.messages = list()
        self.listeners = list()

    def addListener(self, callback):
        self.listeners.append(callback)

    def addMessage(self, user, message):
        self.messages.append((user, message))
        for callback in self.listeners:
            callback(user, message)

class UserThread(threading.Thread):
    def __init__(self, clientsocket, messages):
        threading.Thread.__init__(self)
        self.clientsocket = clientsocket
        self.messages = messages

    def _callback(self, user, message):
        response = f'{user}: {message}'
        self.clientsocket.send(response.encode('utf8'))

    def run(self):
        login = clientsocket.recv(64)
        messages.addListener(lambda _1, _2: self._callback(_1, _2))
        while True:
            data = clientsocket.recv(1024)
            if not data:
                time.sleep(1)
                continue
            messages.addMessage(login.decode('utf8'), data.decode('utf8'))
            print(f'{login}: {data}')


messages = MessageList()

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('', 9099))
sock.listen()

while True:
    clientsocket, addr = sock.accept()
    thread = UserThread(clientsocket, messages)
    thread.start()