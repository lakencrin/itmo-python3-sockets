import socket
import threading

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('localhost', 9099))
login = 'testuser'
sock.send(login.encode('utf8'))

def listen_server(sock):
    print('started')
    while True:
        raw_data = sock.recv(1024)
        if not raw_data:
            time.sleep(1)
            continue
        
        data = raw_data.decode('utf8') 
        current_login = data.split(':')[0]
        if current_login != login: 
            print(data)

def chat(sock):
    while True:
        message = input(f'{login}:')
        sock.send(message.encode('utf8'))


listen_thread = threading.Thread(target=listen_server, args=(sock,), daemon=True)
chat_thread = threading.Thread(target=chat, args=(sock,), daemon=True)

listen_thread.start()
chat_thread.start()

listen_thread.join()
chat_thread.join()
