import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('localhost', 9092))
sock.listen()

while True:
    conn, addr = sock.accept()
    data = conn.recv(4096).decode('UTF-8')

    if not data.startswith('GET / HTTP'):
        continue

    response = '''HTTP/1.1 200 OK
Connection: close
Server: Python TCP Server
Content-Type: text/html; charset=utf-8

'''
    with open('task3/index.html', 'r', encoding='utf8') as html:
        response += html.read()

    conn.send(response.encode('utf-8'))
    conn.close()