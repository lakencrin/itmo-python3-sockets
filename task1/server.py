import socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('localhost', 9090))
sock.listen()

conn, addr = sock.accept()
data = conn.recv(64)
print(data)
conn.send(b'Hello, client')
conn.close()